# csa-lab3

- P33111, Гараев Раиль Фаннурович
- `asm | risc | harv | hw | instr | struct | stream | mem | pstr | prob1`
- упрощенный вариант

# Язык программирования

Особые токены:
```
OPCODE := "st" | "ld" | "mov" | "add" | "sub" | "div" | "rem" | "cmp" | "jmp" | "jne" | "jeq" | "nop" | "halt"
INT_LIT := -?<digit>+ # -1, 2, 342
REG_LIT := r<digit>+ # r1, r2, r123
CHAR_LIT := \'[^\']\' | \'\n\' # 'a', 'b', '.', '\n'
STRING_LIT := \"[^\"]*\" # "", "2", "av"
LABEL_NAME := (<alnum> | _)* # _start, stop, loop

```

Грамматика:
```
program ::= data_section text_section

label ::= LABEL_NAME ':'

data_section ::= "section .data" '\n' data_body
data_body ::= %empty | data_stmt | data_stmt '\n' data_body

data_stmt ::= label data_instr | data_instr

data_instr ::= "word" const_lit | "times" INT_LIT "word" const_lit

const_lit ::= INT_LIT | CHAR_LIT | STRING_LIT

text_section ::= "section .text" '\n' text_body

text_body = %empty | text_stmt | text_stmt '\n' text_body
text_stmt ::= label instr | instr

instr ::= OPCODE | OPCODE arg | OPCODE arg arg | OPCODE arg arg arg

arg ::= lit | LABEL_NAME | "IN" | "OUT"

lit ::= REG_LIT | INT_LIT | CHAR_LIT
```

Говоря менее строго:
- Секция данных
    - Каждая строка либо метка, либо строка описывающая данные
    - После каждой метки обязательно идет не метка
    - Команда описывающая данные - word, в качестве "аргумента" принимает либо число, либо символ, либо строку
        - Строковый литерал означает, что начиная с данной строчки идет pstr, то есть `word "ab"` эквивалентен трем строкам: `word 2`, `word 'a'`, `word 'b'`
    - Перед командой word может стоять конструкция `times <num>`, при помощи которой можно заменить одинаковые подряд идущие команды, `times 2 word 0` == [`word 0`, `word 0`]
- Секция программы
    - Каждая строка либо метка, либо строка описывающая инструкцию
    - После каждой метки обязательно идет не метка
    - Команда описывающая инструкции состоит из Opcode и от 0 до 3 аргументов
    - Аргумент: регистр, число, метка, IN/OUT(конвертируются в адреса устройства ввода и устройства вывода)

Семантика:
- IN/OUT и символьный литерал семантически равны численному аргументу
- Прыгать можно только на текстовые метки(гарвардская архитектура) и значение регистра
- У каждой команды есть [список](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L21) сигнатур, которые они могут принимать
- Как не сложно заметить все команды имеющие результат(кроме `st`) записывают его в регистр.
- Нельзя загружать данные по адресу текстовой метки(гарвардская архитектура)

Дополнительные пояснения:
- В программе должна обязательно быть метка `_start` - входная точка
- `ld` и `mov` отличаются тем, что `mov` воспринимает второй аргумент как значение(`mov r1, label` загрузит в `r1` адрес метки `label`), а `ld` воспринимает второй аргумент как адрес(`ld r1, label` загрузит в `r1` данные по адресу метки `label`)
- `st` воспринимает первый аргумент как адрес, то есть `st r1 r2` загрузит значение регистра `r2` в память по адресу равному значению регистра `r1`
- Только прямая адресация, для косвенной можно сначала при помощи `mov` сложить значение в регистр `r<n>`, а затем применить `ld` куда подать вторым аргументом `r<n>`
- Инструкции ветвления классические - `jmp`, `jne`, `jeq`
- Можно самостоятельно реализовать аналог процедур через переменную в секции данных [golden/hello.yml](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/golden/hello.yml#L17)

# Организация памяти

Размер машинного слова 32 бита. risc, гарвардская архитектура. Виды адресации:
- Непосредственная - аргумент закодирован в команде
- Регистр - аргумент является значением регистра

Регистры доступные программисту: r1 - r4 (число конфигурируется [здесь](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L147) и [здесь](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/machine/datapath.py#L14))

Все регистры: 
- r1-r4 - регистры общего назначения, доступные для программиста
- r5-r6 - регистры общего назначения, недоступны для программиста, используются для имитации immediate value в АЛУ(об этом позже)
- program counter, data address - недоступны программисту и транслятору, внутренние регистры процессора
```
       Instruction memory
+------------------------------+
| 00  : mov r1, x1             |
| 01  : st M1, r1              |
|    (Preload data in mem)     | 
|    ...                       |
| n   : program start          |
|    ...                       |
+------------------------------+

          Data memory
+------------------------------+
| 0x10000000  : input_device   |
| 0x10000001  : output_device  |
|    ...                       |
| 0x20000000 : user data       |
| 0x20000001 : user data       |
|    ...                       |
+------------------------------+
```
Поскольку ЯП - ассемблер => программист практически полностью управляет организацией памяти. Единственное ограничение - он пишет PIC код, что позволяет разместить в памяти устройства пользовательские данные начиная с определенного адреса и выделить отдельные адреса для устройств ввод и вывода. Это позволяет реализовать так называемое MMIO.

Работа с литералами:
1. Символьные литералы и ключевые слова IN/OUT превращаются в `int` еще на этапе парсинга(можно считать, что их заменяет как бы препроцессор)
2. Строковые литералы доступны только в секции данных. Строковый литерал в команде word равносилен нескольким подряд идущим word описывающим строку в формате pstr посимвольно(`word "ab"` == `[word 2, word 'a', word 'b']`).
3. Численные литералы в аргументах команды:
    - АЛУ(в этой реализации) поддерживает операции только над регистрами, для удобства транслятор позволяет использовать immediate value в таких командах как add, sub и тд. Если он обнаруживает что подобная команда содержит immediate value, то он вставляет перед ней инструкции загружающие эти immediate value в регистры и затем выдает нужную команду с временными регистрами. Регистры для этой цели выделены отдельно(они недоступны программисту).
    - ISA поддерживает immediate value в таких командах как: `st`, `ld`, `mov`, `jne`, `jeq`, `jmp`

# Система команд

- [isa.py](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/isa.py)

- Таблица команд:

- Архитектура RISC(reg to reg). Количество аргументов от 0 до 3, аргументом может являться либо регистр, либо непосредственное значение

- Взаимодействие с памятью посредством инструкций `LD` и `ST`. Записать значение из памяти можно только в регистр. Загрузить в память можно только значение регистра.

- Взаимодействие с устройствами ввода/вывода - MMIO. Поэтому для того чтобы прочитать или записать символ достаточно воспользоваться инструкциями `LD` и `ST` с нужным адресом

- Счетчик команд указывает на исполняемую команду, он по умолчанию увеличивается на 1. Можно вмешаться при помощи инструкций безусловного(`JMP`) и условного(`JEQ`, `JNE`) перехода. Можно прыгнуть либо на immediate value, либо на значение в регистре.

| Оператор | Арг1 | Арг2 | Арг3 | Влияние на NZ | Описание |
| -------- | ---- | ---- | ---- | ------------- | -------- |
| MOV      | REG  | REG/IMM | - | ? | АРГ1 <-- АРГ2, загружает в первый регистр значение второго аргумента |       
| LD       | REG  | REG/IMM | - | ? | Загружает в регистр значение в памяти по адресу равному второму аргументу  |       
| ST       | REG/IMM | REG | - | - | Загружает значение второго аргумента в ячейку памяти с адресом равным первому аргументу |
| ADD      | REG | REG | REG | + | АРГ1 <-- АРГ2 + АРГ3 |
| SUB      | REG | REG | REG | + | АРГ1 <-- АРГ2 - АРГ3 |
| REM      | REG | REG | REG | + | АРГ1 <-- АРГ2 % АРГ3 |
| DIV      | REG | REG | REG | + | АРГ1 <-- АРГ2 // АРГ3 |
| CMP      | REG | REG | - | + | Установить флаги как при операции АРГ1 - АРГ2 |
| JMP      | REG/IMM | - | - | - | Безусловный переход на инструкцию с номером АРГ1 |
| JEQ      | REG/IMM | - | - | - | Переход на инструкцию с номером АРГ1, если установлен флаг Z |
| JNE      | REG/IMM | - | - | - | Переход на инструкцию с номером АРГ1, если не установлен флаг Z |
| HALT     | - | - | - | - | Окончание симуляции |

Способ кодирования:
```
LD   REG  1          INT  268435456  
```
Сначала код инструкции, затем через пробельные символы аргументы. Аргумент описывается типом: REG или INT(immediate value). Одна инструкция - одна строка

# Транслятор

```
python csa_lab3/translator/main.py <input_code_filename> <output_instrs_filename>
```

Транслятор работает из предположения, что поданный код соответствует грамматике. При обнаружении несоответствие выбрасывается исключение `ValueError`.

Транслятор построчно обрабатывает файл:

0. Вызывается метод `Translator.translate(src: TextIoBase) -> list[Instruction]`
1. Внутри него вызываются [translate_data_section](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L202) и затем [translate_text_section](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L309).
1. Первая строка которую ожидает парсер - `section .data`
2. Затем до тех пор пока он не встретит `section .text` он будет ожидать на входе 3 варианта строк:
    1. Пустая строка - игнорируется
    2. Строка, которая заканчивается на ':' - ожидается, что это метка. Если она соответствует критериям названия меток, то проверяется существует ли такая метка в `data_labels: dict[str, Translator.Label]`. `Translator.Label` - это класс, который содержит в себе название и адрес, он вынесен в отдельный класс для того, чтобы можно было передать ссылку на этот обьект в команду и затем по щелчку добавить значение адреса метки, когда он станет окончательно известен.
    3. Другая строка, ожидается, что это команда описания данных. Просто проверяется что строка соответствует грамматике. Сразу же транслируется в соответствующие инструкции имитирующие загрузчик в ОС. Поскольку `st` не поддерживает запись в память значение не из регистра, то комадны раскрываются так:
    - word <INT> <---> mov r6 <INT>; st <ADDR> r6;
    - word <CHAR> <---> mov r6 <CHAR>; st <ADDR> r6;
    - word <STR> <---> mov r6 <STR_LEN>; st <ADDR> r6; mov r6 <STR>[0]; st (<ADDR> + 1) r6; ...
    - Адреса выбираются последовательно начиная с указанного в конфигурации. (адреса начала ОЗУ)
3. Как только находим `section .text` переключаемся в другой режим, на вход ожидаются 3 варианта строк:
    1. Пустая строка - игнорируется
    2. Строка, которая заканчивается на ':' - ожидается, что это метка. Аналогично, только `Translator.Label` дополнительно добавляется в список возвращаемый `translate_text_section`(он возвращает `list[Translator.Label | Translator.InnerInstruction]`).
    3. Другая строка - ожидается, что это инструкция. Такую строку транслятор пытается превратить в `Translator.InnerInstruction` (отличие от обычной инструкции в том, что она может содержать аргументы типа метка, а не только регистр или непосредственное значение). Сначала просто считывается opcode, а затем считываются все переданные аргументы. После получения `Translator.InnerInstruction` происходит валидация аргументов, на то соответствуют ли они доступным для данной инструкции сигнатурам.
4. На выходе получаем смесь `Translator.Label` и `Translator.InnerInstruction`. Перед тем как перейти к раскрытию меток нужно совершить еще один [шаг](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/translator/translator.py#L263). ALU принимает только значение из регистров, поэтому любые команды использующие immediate value должны быть дополнительно обработаны. А именно происходит раскрытие по типу: op r1, IMM1, IMM2 --> mov r6, IMM1; mov r5, IMM2; op r1, r5, r6
5. Теперь избавимся от меток. Идем по списку, если метка, то заполняем ее адрес, выкидываем метку из списка.
6. Еще раз идем по списку `InnerInstruction`, если это команда, не содержащая метки, то просто переводим ее в `Instruction`. Если она содержит метки, то метки заменяем на аргументы типа INT со значением адреса метки и так же переводим в `Instruction`.
7. Работа транслятора на этом окончена(просто выводим полученные инструкции с `enumerate()`)

# Модель процессора
```
python csa_lab3/machine/main.py <FILE_WITH_COMMANDS> <FILE_WITH_INPUT>
```

![Control Unit](control_unit.png)

Сигналы:
- `sel_next` - мультиплексор на вход в `Program Counter`, либо инкремент текущего значения, либо `control_unit_arg` (поддержка прыжка по значению регистра)
- `control_unit_arg` - получается из мультиплексора, который управляется `sel_arg`. Либо immediate value из инструкции, либо вывод alu_output из datapath
- `latch_pc` - сохранить значение на входе в `Program Counter`

![Datapath](datapath.png)

Сигналы:
- `latch_data_addr` - сохранить значение на входе в `Data Address`
- `WR` - записать значение на входе в `data_memory` или в `output_device` в зависимости от `CS`
- `OE` - включит вывод значения из `data_memory` или `input_device` в зависимости от `CS`
- `sel_reg_input` - мультиплексор на входе в регистровый файл. Либо значение из памяти/устройства, либо `control_unit_arg` - может быть immediate value(а может быть значением другого регистра)
- `sel_lreg`, `sel_rreg`, `sel_ireg` - выбрать регистр (левый, правый или регистр, в который записывают)
- `latch_ireg` - сохранить значение на входе в регистровый файл, в выбранный регистр для записи
- `alu_signals` - выбор операции в ALU

Особенности программной реализации:
- Основной класс для связывания нескольких обьектов эмулирующих элементы схемы - `Wire`. Особенность `Wire` заключается в том, что на него можно подписаться и тогда, когда кто то захочет поменять в нем значение будут вызваны все зарегистрированные колбеки. Это позволяет с легкостью делать сколь угодно сложные схемы без боязни забыть положить куда-либо новое значение. Всё что нужно это добавить в конструкторе логического элемента подписать его на нужные провода передаваемые в конструктор и создать выходные провода, чтобы на них можно было прицепить новые элементы.
- Также на базе проводов реализован класс `Mux` и `WireUnion`(обьединение двух проводов в один). [все это реализовано здесь](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/csa_lab3/machine/common.py)


# Тестирование

Тесты:
1. [cat](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/golden/cat.yml)
2. [hello world](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/golden/hello.yml)
3. [hello user name](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/golden/hello_user_name.yml)
4. [prob1](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/golden/prob1.yml) - сумма всех чисел кратных 3 или 5 до числа 1000

Для каждого теста выполняется: [test_translator_and_machine](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/tests/test_translator.py)


CI при помощи gitlab:

``` yaml
lab3-example:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
    - ruff check .
```

где:

- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

[pyproject.toml](https://gitlab.se.ifmo.ru/railolog/csa-lab3/-/blob/master/pyproject.toml) - настройки зависимостей

Пример использования на примере программы cat:
```
< cat .\programs\cat.asm
> section .data
> data:
>    word 0      
> section .text   
> 
> _start:
>     nop
> loop:
>     ld r1, INP  
>     st OUT, r1  
>     cmp r1, '\n'
>     jne loop    
> end:
>     halt    
< python .\csa_lab3\translator\main.py .\programs\cat.asm .\programs\cat.cmds
< cat .\programs\cat.cmds
> 0   JMP  INT  1
> 1   NOP  
> 2   LD   REG  1          INT  268435456  
> 3   ST   INT  268435457  REG  1
> 4   MOV  REG  5          INT  10
> 5   CMP  REG  1          REG  5
> 6   JNE  INT  2
> 7   HALT 
< cat .\programs\cat.input
> cat
< python .\csa_lab3\machine\main.py programs\cat.cmds programs\cat.input
> cat
> 
< cat out.log
> TICK:     1 CMD: [JMP  INT  1          ] PC:   0
>  R1:        0
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:        0
>  R6:        0
>  DATA_ADDR:          0 N|Z: 0|1
> TICK:     3 CMD: [NOP  ] PC:   1
>  R1:        0
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:        0
>  R6:        0
>  DATA_ADDR:          0 N|Z: 0|1
> TICK:     5 CMD: [LD   REG  1          INT  268435456  ] PC:   2
>  R1:        0
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:        0
>  R6:        0
>  DATA_ADDR:          0 N|Z: 0|1
> input: ['\n', 't', 'a'] --> 'c'
> TICK:     8 CMD: [ST   INT  268435457  REG  1          ] PC:   3
>  R1:       99
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:        0
>  R6:        0
>  DATA_ADDR:  268435456 N|Z: 0|1
> output: [] <-- 'c'
> TICK:    11 CMD: [MOV  REG  5          INT  10         ] PC:   4
>  R1:       99
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:        0
>  R6:        0
>  DATA_ADDR:  268435457 N|Z: 0|0
> TICK:    14 CMD: [CMP  REG  1          REG  5          ] PC:   5
>  R1:       99
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|0
> TICK:    16 CMD: [JNE  INT  2          ] PC:   6
>  R1:       99
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|0
... чтение и вывод 'a' и 't'
> TICK:    44 CMD: [LD   REG  1          INT  268435456  ] PC:   2
>  R1:      116
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|0
> input: [] --> '
> '
> TICK:    47 CMD: [ST   INT  268435457  REG  1          ] PC:   3
>  R1:       10
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:  268435456 N|Z: 0|1
> output: ['c', 'a', 't'] <-- '
> '
> TICK:    50 CMD: [MOV  REG  5          INT  10         ] PC:   4
>  R1:       10
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:  268435457 N|Z: 0|0
> TICK:    53 CMD: [CMP  REG  1          REG  5          ] PC:   5
>  R1:       10
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|0
> TICK:    55 CMD: [JNE  INT  2          ] PC:   6
>  R1:       10
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|1
> TICK:    57 CMD: [HALT ] PC:   7
>  R1:       10
>  R2:        0
>  R3:        0
>  R4:        0
>  R5:       10
>  R6:        0
>  DATA_ADDR:         10 N|Z: 0|1
```

Пример проверки исходного кода: 
```
< poetry run pytest --update-goldens -v
> =================================================================== test session starts > ====================================================================
> platform win32 -- Python 3.11.0, pytest-7.4.4, pluggy-1.4.0 -- D:\Programming\csa\csa-lab3\.venv\Scripts\python.exe
> cachedir: .pytest_cache
> rootdir: D:\Programming\csa\csa-lab3
> configfile: pyproject.toml
> testpaths: tests
> plugins: golden-0.2.2
> collected 4 items
> 
> tests/test_translator.py::test_translator_and_machine[../golden/cat.yml] > PASSED                                                                       [ 25%]
> tests/test_translator.py::test_translator_and_machine[../golden/hello.yml] > PASSED                                                                     [ 50%]
> tests/test_translator.py::test_translator_and_machine[../golden/hello_world.yml] > PASSED                                                               [ 75%]
> tests/test_translator.py::test_translator_and_machine[../golden/prob1.yml] > PASSED                                                                     [100%]
> 
> ==================================================================== 4 passed in 16.98s > ==================================================================== 
< poetry run ruff check .
< poetry run ruff format .
> 12 files left unchanged
```

```
| ФИО | <алг> | <LoC> | <code байт> | <code инстр.> | <инстр.> | <такт.> | <вариант> |
| Гараев Раиль Фаннурович | cat | 11 | - | 8 | 25 | 57 | asm | risc | harv | hw | instr | struct | stream | mem | pstr | prob1 |
| Гараев Раиль Фаннурович | hello | 63 | - | 165 | 590 | 1608 | asm | risc | harv | hw | instr | struct | stream | mem | pstr | prob1 |
| Гараев Раиль Фаннурович | prob1 | 44 | - | 68 | 14960 | 38511 | asm | risc | harv | hw | instr | struct | stream | mem | pstr | prob1 |
```
