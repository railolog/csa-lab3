section .data
buf:    
    times 10 word 0

section .text
_start:
    mov r1, 0
    mov r2, 0
loop:
    add r2, r2, 1
    cmp r2, 1000
    jeq print_ans
    rem r3, r2, 3
    cmp r3, 0
    jeq add
    rem r3, r2, 5
    cmp r3, 0
    jeq add
    jmp loop
add:
    add r1, r1, r2
    jmp loop
print_ans:
    mov r2, 0
write_ans_loop:
    rem r3, r1, 10
    div r1, r1, 10
    add r4, buf, r2
    st r4, r3
    cmp r1, 0
    jeq print_ans_loop
    add r2, r2, 1
    jmp write_ans_loop
print_ans_loop:
    add r1, buf, r2
    ld r1, r1
    add r1, r1, '0'
    st OUT, r1
    cmp r2, 0
    jeq end
    sub r2, r2, 1
    jmp print_ans_loop
end:
    halt