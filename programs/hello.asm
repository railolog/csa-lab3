section .data
quest:  
    word "What is your name?:"
hello:  
    word "hello, "
len:    
    word 0
buf:    
    times 30 word 0
bang:   
    word '!'
ret:   
    word 0

section .text
print_str:
    ld r2, r1
print_str_loop:
    cmp r2, 0
    jeq print_str_end
    sub r2, r2, 1
    add r1, r1, 1
    ld r3, r1
    st OUT, r3
    jmp print_str_loop
print_str_end:
    ld r1 ret
    jmp r1

_start:
    nop
print_question:
    mov r1, quest
    mov r2, input_name
    st ret, r2
    jmp print_str
input_name:
    mov r3, 0
    mov r1, buf
input_loop:
    ld r2, INP
    cmp r2, '\n'
    jeq input_end
    st r1, r2
    add r1, r1, 1
    add r3, r3, 1
    jmp input_loop
input_end:
    st len, r3
print_hello:
    mov r1, hello
    mov r2, print_buf
    st ret, r2
    jmp print_str
print_buf:
    mov r1, len
    mov r2, print_bang
    st ret, r2
    jmp print_str
print_bang:
    ld r1, bang
    st OUT, r1
    halt