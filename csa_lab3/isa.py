from __future__ import annotations

import enum


@enum.unique
class ArgType(enum.Flag):
    REG = enum.auto()
    INT = enum.auto()


class Argument:
    def __init__(self, arg_type: ArgType, value: int) -> None:
        self.arg_type = arg_type
        self.value = value


@enum.unique
class Opcode(enum.Enum):
    ST = enum.auto()
    LD = enum.auto()

    MOV = enum.auto()

    ADD = enum.auto()
    SUB = enum.auto()
    DIV = enum.auto()
    REM = enum.auto()

    CMP = enum.auto()

    JMP = enum.auto()
    JNE = enum.auto()
    JEQ = enum.auto()

    NOP = enum.auto()
    HALT = enum.auto()


class Instruction:
    def __init__(self, opcode: Opcode, args: list[Argument]) -> None:
        self.opcode = opcode
        self.args = args

    def __str__(self) -> str:
        res = f"{self.opcode.name:<4} "
        for arg in self.args:
            res += f"{arg.arg_type.name:<4} {arg.value:<10} "
        return res

    @staticmethod
    def parse(line: str) -> Instruction:
        words = line.split()
        return Instruction(Opcode[words[0]], [Argument(ArgType[x], int(y)) for x, y in zip(words[1::2], words[2::2])])
