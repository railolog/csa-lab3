from __future__ import annotations

import enum
import logging
from types import MappingProxyType

from csa_lab3.isa import ArgType, Argument, Instruction, Opcode
from csa_lab3.machine.common import Mux, Wire
from csa_lab3.machine.datapath import AluOp, Datapath


class ControlUnit:
    class ArgSel(enum.Enum):
        ALU_OUTPUT = enum.auto()
        IMMEDIATE_ARG = enum.auto()

    class PcSel(enum.Enum):
        CONTROL_UNIT_ARG = enum.auto()
        PC_INC = enum.auto()

    def sel_arg(self, value: ArgSel):
        self.arg_sel = value
        self.arg_mux.update()

    def update_pc_in(self, value: int):
        self.pc_in = value

    def select_immediate_value(self, instr: Instruction):
        res: int = 0
        match instr.opcode:
            case Opcode.ST | Opcode.JMP | Opcode.JNE | Opcode.JEQ:
                res = instr.args[0].value
            case Opcode.LD | Opcode.MOV:
                res = instr.args[1].value
            case _:
                res = 0xDEADBEEF
        self.imm_arg_wire.set_value(res)

    def latch_pc(self):
        self.pc = self.pc_in
        self.pc_wire_inc.set_value(self.pc + 1)
        self.current_instruction = self.instructions[self.pc]
        self.select_immediate_value(self.current_instruction)

    def sel_pc(self, value: PcSel):
        self.pc_sel = value
        self.pc_mux.update()

    def __init__(self, instructions: list[Instruction], input_buffer: list[str]) -> None:
        self.imm_arg_wire: Wire = Wire()

        self.arg_sel: ControlUnit.ArgSel = ControlUnit.ArgSel.IMMEDIATE_ARG

        alu_wire: Wire = Wire()
        self.arg_mux: Mux = Mux(alu_wire, self.imm_arg_wire, lambda x=self: x.arg_sel == ControlUnit.ArgSel.ALU_OUTPUT)

        self.datapath: Datapath = Datapath(self.arg_mux.output, self.arg_mux.output, input_buffer)
        self.datapath.alu.alu_output.subscribe(lambda value, x=alu_wire: x.set_value(value))

        self.pc: int = -1
        self.pc_sel: ControlUnit.PcSel = ControlUnit.PcSel.PC_INC
        self.pc_wire_inc: Wire = Wire()

        self.pc_mux: Mux = Mux(
            self.pc_wire_inc, self.arg_mux.output, lambda x=self: x.pc_sel == ControlUnit.PcSel.PC_INC
        )
        self.pc_in: int = 0

        self.pc_mux.output.subscribe(self.update_pc_in)

        self.current_instruction: Instruction = instructions[0]
        self.instructions = instructions

        self.ticks = 0

    def get_control_unit_arg(self, arg: Argument):
        if arg.arg_type != ArgType.REG:
            self.sel_arg(ControlUnit.ArgSel.IMMEDIATE_ARG)
        else:
            self.datapath.registers.sel_lreg(arg.value)
            self.datapath.alu.set_op(AluOp.PASS_L)
            self.sel_arg(ControlUnit.ArgSel.ALU_OUTPUT)

    opcode_to_alu_op = MappingProxyType(
        {
            Opcode.ADD: AluOp.ADD,
            Opcode.SUB: AluOp.SUB,
            Opcode.DIV: AluOp.DIV,
            Opcode.REM: AluOp.REM,
            Opcode.CMP: AluOp.CMP,
        }
    )

    def fetch_decode_execute(self):
        # clear flags
        self.datapath.set_oe(False)
        self.datapath.set_wr(False)

        self.latch_pc()
        self.sel_pc(ControlUnit.PcSel.PC_INC)

        self.ticks += 1

        logging.debug("%s", self)

        instr = self.current_instruction
        match instr.opcode:
            case Opcode.ST:
                self.get_control_unit_arg(instr.args[0])
                self.datapath.latch_data_addr()
                self.ticks += 1

                assert instr.args[1].arg_type == ArgType.REG
                self.datapath.registers.sel_lreg(instr.args[1].value)
                self.datapath.alu.set_op(AluOp.PASS_L)
                self.datapath.set_wr(True)
                self.ticks += 1
            case Opcode.LD:
                self.get_control_unit_arg(instr.args[1])
                self.datapath.latch_data_addr()
                self.ticks += 1

                assert instr.args[0].arg_type == ArgType.REG
                self.datapath.registers.sel_ireg(instr.args[0].value)
                self.datapath.sel_reg_input(Datapath.RegInput.DATA_OUT)
                self.datapath.set_oe(True)
                self.datapath.registers.latch_ireg()
                self.ticks += 1
            case Opcode.MOV:
                self.get_control_unit_arg(instr.args[1])
                self.ticks += 1

                assert instr.args[0].arg_type == ArgType.REG
                self.datapath.registers.sel_ireg(instr.args[0].value)
                self.datapath.sel_reg_input(Datapath.RegInput.CONTROL_UNIT_ARG)
                self.datapath.registers.latch_ireg()
                self.ticks += 1
            case Opcode.ADD | Opcode.SUB | Opcode.DIV | Opcode.REM:
                assert instr.args[1].arg_type == ArgType.REG
                self.datapath.registers.sel_lreg(instr.args[1].value)
                assert instr.args[2].arg_type == ArgType.REG
                self.datapath.registers.sel_rreg(instr.args[2].value)

                self.datapath.alu.set_op(self.opcode_to_alu_op[instr.opcode])
                self.ticks += 1

                assert instr.args[0].arg_type == ArgType.REG
                self.datapath.registers.sel_ireg(instr.args[0].value)
                self.sel_arg(ControlUnit.ArgSel.ALU_OUTPUT)
                self.datapath.sel_reg_input(Datapath.RegInput.CONTROL_UNIT_ARG)
                self.datapath.registers.latch_ireg()
                self.ticks += 1
            case Opcode.CMP:
                assert instr.args[0].arg_type == ArgType.REG
                self.datapath.registers.sel_lreg(instr.args[0].value)
                assert instr.args[1].arg_type == ArgType.REG
                self.datapath.registers.sel_rreg(instr.args[1].value)

                self.datapath.alu.set_op(self.opcode_to_alu_op[instr.opcode])
                self.ticks += 1
            case Opcode.JEQ | Opcode.JNE | Opcode.JMP:
                self.get_control_unit_arg(instr.args[0])
                if instr.opcode == Opcode.JEQ and self.datapath.alu.Z and not self.datapath.alu.N:
                    self.sel_pc(ControlUnit.PcSel.CONTROL_UNIT_ARG)
                if instr.opcode == Opcode.JNE and not self.datapath.alu.Z:
                    self.sel_pc(ControlUnit.PcSel.CONTROL_UNIT_ARG)
                if instr.opcode == Opcode.JMP:
                    self.sel_pc(ControlUnit.PcSel.CONTROL_UNIT_ARG)
                self.ticks += 1
            case Opcode.NOP:
                self.ticks += 1
            case Opcode.HALT:
                raise StopIteration()

    def __str__(self) -> str:
        res = f"TICK: {self.ticks:5} CMD: [{self.current_instruction}] PC: {self.pc:3}\n"
        for k in range(1, self.datapath.registers.reg_cnt + 1):
            res += f" R{k}: {self.datapath.registers.mem.get(k, 0):>8}\n"
        res += f" DATA_ADDR: {self.datapath.data_addr:10} N|Z: {int(self.datapath.alu.N)}|{int(self.datapath.alu.Z)}"

        return res
