from __future__ import annotations

from typing import Callable


class Wire:
    def __init__(self) -> None:
        self.callbacks: list[Callable[[int], None]] = []

    def subscribe(self, callback: Callable[[int], None]) -> None:
        self.callbacks.append(callback)

    def set_value(self, value: int):
        for cb in self.callbacks:
            cb(value)


class WireUnion(Wire):
    def __init__(self, lhs: Wire, rhs: Wire) -> None:
        super().__init__()
        lhs.subscribe(super().set_value)
        rhs.subscribe(super().set_value)


class Mux:
    def update(self):
        if self.choose_left():
            self.output.set_value(self.lhs)
        else:
            self.output.set_value(self.rhs)

    def update_lhs(self, value: int):
        self.lhs = value
        self.update()

    def update_rhs(self, value: int):
        self.rhs = value
        self.update()

    def __init__(self, lhs: Wire, rhs: Wire, choose_left: Callable[[], bool]) -> None:
        self.lhs: int = 0
        lhs.subscribe(self.update_lhs)

        self.rhs: int = 0
        rhs.subscribe(self.update_rhs)

        self.choose_left = choose_left

        self.output: Wire = Wire()
