from __future__ import annotations

import logging
import sys

from csa_lab3.isa import Instruction
from csa_lab3.machine.control_unit import ControlUnit


def main(code_file: str, input_file: str):
    with open("out.log", "w+") as f:
        logging.basicConfig(stream=f, level=logging.DEBUG, format="%(message)s")

        with open(code_file) as code:
            instrs = [Instruction.parse(line.split(maxsplit=1)[1]) for line in code.readlines()]
        with open(input_file) as inp:
            input_data = [*(inp.read())]
        control_unit = ControlUnit(instrs, input_data)
        try:
            while True:
                control_unit.fetch_decode_execute()
        except StopIteration:
            pass
        print("".join(control_unit.datapath.output.output_buffer))


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
