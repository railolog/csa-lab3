from __future__ import annotations

import enum
import logging

from csa_lab3.machine.common import Mux, Wire, WireUnion


class RegisterFile:
    def update_ireg_input(self, value: int):
        self.ireg_input = value

    def __init__(self, input_ireg: Wire) -> None:
        self.reg_cnt = 6

        self.mem: dict[int, int] = dict()

        self.ireg_idx: int = 0
        self.ireg_input: int = 0
        input_ireg.subscribe(self.update_ireg_input)

        self.lreg_idx = 0
        self.lreg_out: Wire = Wire()

        self.rreg_idx = 0
        self.rreg_out: Wire = Wire()

    def sel_ireg(self, idx: int):
        self.ireg_idx = idx

    def latch_ireg(self):
        assert 0 < self.ireg_idx <= self.reg_cnt
        self.mem[self.ireg_idx] = self.ireg_input

        if self.ireg_idx == self.lreg_idx:
            self.lreg_out.set_value(self.ireg_input)
        if self.ireg_idx == self.rreg_idx:
            self.rreg_out.set_value(self.ireg_input)

    def sel_lreg(self, idx: int):
        assert 0 < idx <= self.reg_cnt
        self.lreg_idx = idx
        self.lreg_out.set_value(self.mem[idx])

    def sel_rreg(self, idx: int):
        assert 0 < idx <= self.reg_cnt
        self.rreg_idx = idx
        self.rreg_out.set_value(self.mem[idx])


@enum.unique
class AluOp(enum.Enum):
    PASS_L = enum.auto()

    ADD = enum.auto()
    SUB = enum.auto()
    DIV = enum.auto()
    REM = enum.auto()

    CMP = enum.auto()


class ALU:
    def update(self):
        res: int = 0
        match self.op:
            case AluOp.PASS_L:
                res = self.lhs
            case AluOp.ADD:
                res = self.lhs + self.rhs
            case AluOp.SUB:
                res = self.lhs - self.rhs
            case AluOp.DIV:
                if self.rhs == 0:
                    res = 0xDEADBEEF
                else:
                    res = self.lhs // self.rhs
            case AluOp.REM:
                if self.rhs == 0:
                    res = 0xDEADBEEF
                else:
                    res = self.lhs % self.rhs
            case AluOp.CMP:
                res = self.lhs - self.rhs

        self.N = res < 0
        self.Z = res == 0

        self.alu_output.set_value(res)

    def update_l(self, new_lhs: int):
        self.lhs = new_lhs
        self.update()

    def update_r(self, new_rhs: int):
        self.rhs = new_rhs
        self.update()

    def set_op(self, new_op: AluOp):
        self.op = new_op
        self.update()

    def __init__(self, lhs_in: Wire, rhs_in: Wire) -> None:
        self.op: AluOp = AluOp.PASS_L
        self.lhs = 0
        self.rhs = 0

        self.N = False
        self.Z = True

        self.alu_output: Wire = Wire()

        lhs_in.subscribe(self.update_l)
        rhs_in.subscribe(self.update_r)


class InputDevice:
    def __init__(self, input_buffer: list[str]) -> None:
        self.data_out: Wire = Wire()
        self.OE = False
        self.CS = False
        self.input_buffer = input_buffer[::-1]

    def set_cs(self, cs: bool):
        self.CS = cs

    def set_oe(self, oe: bool):
        if not self.OE:
            if oe:
                self.OE = oe
                if self.CS:
                    value = self.input_buffer.pop()
                    self.data_out.set_value(ord(value))
                    logging.debug(f"input: {self.input_buffer} --> '{value}'")
        self.OE = oe


class OutputDevice:
    def update_data_in(self, value):
        self.data_in = value

    def __init__(self, data_in: Wire) -> None:
        self.data_in: int = 0
        data_in.subscribe(self.update_data_in)

        self.CS = False
        self.WR = False

        self.output_buffer: list[str] = []

    def set_cs(self, cs: bool):
        self.CS = cs

    def set_wr(self, wr: bool):
        if not self.WR:
            if wr:
                self.WR = wr
                if self.CS:
                    logging.debug(f"output: {self.output_buffer} <-- '{chr(self.data_in)}'")
                    self.output_buffer.append(chr(self.data_in))
        self.WR = wr


class Memory:
    def update_data_in(self, value: int):
        self.data_in = value

    def update_data_addr(self, value: int):
        self.data_addr = value

    def __init__(self, data_in: Wire, data_addr: Wire) -> None:
        self.mem: dict[int, int] = dict()

        self.data_in: int = 0
        data_in.subscribe(self.update_data_in)
        self.data_addr: int = 0
        data_addr.subscribe(self.update_data_addr)

        self.data_out: Wire = Wire()

        self.WR = False
        self.OE = False
        self.CS = False

    def set_wr(self, wr: bool):
        if not self.WR:
            if wr:
                self.WR = wr
                if self.CS:
                    self.mem[self.data_addr] = self.data_in
        self.WR = wr

    def set_oe(self, oe: bool):
        if not self.OE:
            if oe:
                self.OE = oe
                if self.CS:
                    self.data_out.set_value(self.mem[self.data_addr])
        self.OE = oe

    def set_cs(self, cs: bool):
        self.CS = cs


class Datapath:
    class RegInput(enum.Enum):
        CONTROL_UNIT_ARG = enum.auto()
        DATA_OUT = enum.auto()

    def update_data_addr_in(self, value: int):
        self.data_addr_in = value

    def update_arg(self, value: int):
        self.arg = value

    def __init__(self, control_unit_arg: Wire, data_addr: Wire, input_buffer: list[str]) -> None:
        self.input_addr = 0x10000000
        self.output_addr = 0x10000001

        self.data_addr_in: int = 0
        data_addr.subscribe(self.update_data_addr_in)

        self.arg: int = 0
        control_unit_arg.subscribe(self.update_arg)

        self.data_addr: int = 0
        self.data_addr_wire: Wire = Wire()

        self.input = InputDevice(input_buffer)

        alu_wire: Wire = Wire()
        self.mem = Memory(alu_wire, self.data_addr_wire)

        self.output = OutputDevice(alu_wire)

        data_out_wire: Wire = WireUnion(self.input.data_out, self.mem.data_out)

        self.reg_input: Datapath.RegInput = Datapath.RegInput.CONTROL_UNIT_ARG

        self.reg_input_mux: Mux = Mux(
            data_out_wire, control_unit_arg, lambda x=self: x.reg_input == Datapath.RegInput.DATA_OUT
        )

        self.registers = RegisterFile(self.reg_input_mux.output)

        self.alu = ALU(self.registers.lreg_out, self.registers.rreg_out)

        self.alu.alu_output.subscribe(lambda value, x=alu_wire: x.set_value(value))

    def sel_reg_input(self, value: Datapath.RegInput):
        self.reg_input = value
        self.reg_input_mux.update()

    def latch_data_addr(self):
        self.data_addr_wire.set_value(self.data_addr_in)
        self.data_addr = self.data_addr_in
        if self.data_addr_in == self.input_addr:
            self.mem.set_cs(False)
            self.input.set_cs(True)
            self.output.set_cs(False)
        elif self.data_addr_in == self.output_addr:
            self.mem.set_cs(False)
            self.input.set_cs(False)
            self.output.set_cs(True)
        else:
            self.mem.set_cs(True)
            self.input.set_cs(False)
            self.output.set_cs(False)

    def set_oe(self, oe):
        self.mem.set_oe(oe)
        self.input.set_oe(oe)

    def set_wr(self, wr):
        self.mem.set_wr(wr)
        self.output.set_wr(wr)
