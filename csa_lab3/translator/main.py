import sys

from csa_lab3.translator.translator import Translator


def main(src: str, dst: str):
    translator = Translator()
    with open(src) as fin:
        instrs = translator.translate(fin)
    with open(dst, "w+") as fout:
        for i, instr in enumerate(instrs):
            print(f"{i:<3} {instr}", file=fout)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
