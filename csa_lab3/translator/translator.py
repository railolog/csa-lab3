from __future__ import annotations

import enum
from io import TextIOBase
from types import MappingProxyType

from csa_lab3.isa import ArgType, Argument, Instruction, Opcode


class Translator:
    @enum.unique
    class InnerArgType(enum.Flag):
        REG = enum.auto()
        INT = enum.auto()
        DATA_LABEL = enum.auto()
        TEXT_LABEL = enum.auto()
        ANY = REG | INT | DATA_LABEL | TEXT_LABEL
        REG_INT_DATA = REG | INT | DATA_LABEL
        LABEL = DATA_LABEL | TEXT_LABEL

    signatures = MappingProxyType(
        {
            Opcode.ST: [[InnerArgType.REG_INT_DATA, InnerArgType.REG]],
            Opcode.LD: [[InnerArgType.REG, InnerArgType.REG_INT_DATA]],
            Opcode.MOV: [[InnerArgType.REG, InnerArgType.ANY]],
            Opcode.ADD: [[InnerArgType.REG, InnerArgType.ANY, InnerArgType.ANY]],
            Opcode.SUB: [[InnerArgType.REG, InnerArgType.ANY, InnerArgType.ANY]],
            Opcode.DIV: [[InnerArgType.REG, InnerArgType.ANY, InnerArgType.ANY]],
            Opcode.REM: [[InnerArgType.REG, InnerArgType.ANY, InnerArgType.ANY]],
            Opcode.CMP: [[InnerArgType.ANY, InnerArgType.ANY]],
            Opcode.JNE: [[InnerArgType.TEXT_LABEL | InnerArgType.REG]],
            Opcode.JEQ: [[InnerArgType.TEXT_LABEL | InnerArgType.REG]],
            Opcode.JMP: [[InnerArgType.TEXT_LABEL | InnerArgType.REG]],
        }
    )

    class Label:
        def __init__(self, name: str) -> None:
            self.name = name
            self.addr: int = -1

        @staticmethod
        def parse(translator: Translator, line: str) -> Translator.Label:
            if line[-1] != ":":
                raise ValueError()

            if any(map(lambda x: not x.isalnum() and x != "_", line[:-1])):
                raise ValueError()

            return Translator.Label(line[:-1])

    class InnerArgument:
        def __init__(self, arg_type: Translator.InnerArgType, value: int | Translator.Label) -> None:
            self.arg_type = arg_type
            self.value = value

        @staticmethod
        def parse_label_arg(translator: Translator, line: str) -> Translator.InnerArgument:
            if any(map(lambda x: not x.isalnum() and x != "_", line[:-1])):
                raise ValueError()

            if line in translator.data_labels:
                arg_type = Translator.InnerArgType.DATA_LABEL
                value = translator.data_labels[line]
            elif line in translator.text_labels:
                arg_type = Translator.InnerArgType.TEXT_LABEL
                value = translator.text_labels[line]
            else:
                arg_type = Translator.InnerArgType.TEXT_LABEL
                value = Translator.Label(line)
                translator.text_labels[line] = value
            return Translator.InnerArgument(arg_type, value)

        @staticmethod
        def parse(translator: Translator, line: str) -> Translator.InnerArgument:
            line = line[:-1] if line[-1] == "," else line
            line = line[1:] if line[0] == "," else line

            res: Translator.InnerArgument | None = None

            if line[0] == "r" and all(map(lambda x: x.isdigit(), line[1:])):
                value = int(line[1:])
                if value > translator.max_reg_num:
                    raise ValueError()
                res = Translator.InnerArgument(Translator.InnerArgType.REG, value)

            if (
                res is None
                and all(map(lambda x: x.isdigit(), line[1:]))
                and (line[0] in ["-", "+"] or line[0].isdigit())
            ):
                res = Translator.InnerArgument(Translator.InnerArgType.INT, int(line))

            if res is None and line[0] == "'" and line[-1] == "'":
                line = "\n" if line[1:-1] == "\\n" else line[1:-1]
                res = Translator.InnerArgument(Translator.InnerArgType.INT, ord(line))

            if res is None and line in ["INP", "OUT"]:
                res = Translator.InnerArgument(
                    Translator.InnerArgType.INT, translator.input_addr if line == "INP" else translator.output_addr
                )

            if res is None:
                res = Translator.InnerArgument.parse_label_arg(translator, line)

            return res

    class InnerInstruction:
        def __init__(self, opcode: Opcode, args: list[Translator.InnerArgument]) -> None:
            self.opcode = opcode
            self.args = args

        @staticmethod
        def parse(translator: Translator, line: str) -> Translator.InnerInstruction:
            words: list[str] = line.split()

            opcode = Opcode[words[0].upper()]

            args: list[Translator.InnerArgument] = []
            for w in words[1:]:
                args.append(Translator.InnerArgument.parse(translator, w))

            return Translator.InnerInstruction(opcode, args)

        def check_signature(self) -> bool:
            if self.opcode in [Opcode.NOP, Opcode.HALT]:
                return len(self.args) == 0

            signatures = Translator.signatures[self.opcode]
            for s in signatures:
                if len(self.args) == len(s):
                    broken = False
                    for arg, arg_type in zip(self.args, s):
                        if arg.arg_type not in arg_type:
                            broken = True
                            break
                    if not broken:
                        return True
            return False

    def __init__(self) -> None:
        self.input_addr = 0x10000000
        self.output_addr = 0x10000001

        self.data_start = 0x20000000

        self.reg_cnt = 6
        self.max_reg_num = self.reg_cnt - 2

        self.data_labels: dict[str, Translator.Label] = dict()
        self.text_labels: dict[str, Translator.Label] = dict()

    def translate_const_lit(self, line: str, addr_cnt: int) -> tuple[list[Instruction], int]:
        line = line.strip()
        if line[0] == '"':
            if line[-1] != '"':
                print(line)
                raise ValueError()
            value = line[1:-1]
            instrs: list[Instruction] = [
                Instruction(Opcode.MOV, [Argument(ArgType.REG, self.reg_cnt), Argument(ArgType.INT, len(value))]),
                Instruction(
                    Opcode.ST, [Argument(ArgType.INT, self.data_start + addr_cnt), Argument(ArgType.REG, self.reg_cnt)]
                ),
            ]
            for i, s in enumerate(value):
                instrs.append(
                    Instruction(Opcode.MOV, [Argument(ArgType.REG, self.reg_cnt), Argument(ArgType.INT, ord(s))])
                )
                instrs.append(
                    Instruction(
                        Opcode.ST,
                        [
                            Argument(ArgType.INT, self.data_start + addr_cnt + i + 1),
                            Argument(ArgType.REG, self.reg_cnt),
                        ],
                    ),
                )
            return instrs, len(value) + 1

        if line[0] == "'":
            if line[-1] != "'":
                raise ValueError()
            value = line[1:-1]
            if value == "\\n":
                value = "\n"
            return [
                Instruction(Opcode.MOV, [Argument(ArgType.REG, self.reg_cnt), Argument(ArgType.INT, ord(value))]),
                Instruction(
                    Opcode.ST, [Argument(ArgType.INT, self.data_start + addr_cnt), Argument(ArgType.REG, self.reg_cnt)]
                ),
            ], 1

        value = int(line)
        return [
            Instruction(Opcode.MOV, [Argument(ArgType.REG, self.reg_cnt), Argument(ArgType.INT, value)]),
            Instruction(
                Opcode.ST, [Argument(ArgType.INT, self.data_start + addr_cnt), Argument(ArgType.REG, self.reg_cnt)]
            ),
        ], 1

    def translate_data_section(self, src: TextIOBase) -> list[Instruction]:
        src.readline().strip()

        res: list[Instruction] = []
        addr_cnt = 0

        line = src.readline().strip()
        while line != "section .text":
            if not line:
                line = src.readline().strip()
                continue

            if line.endswith(":"):
                label = Translator.Label.parse(self, line)
                label.addr = self.data_start + addr_cnt
                self.data_labels[label.name] = label
                line = src.readline().strip()
                continue

            word, line = line.split(maxsplit=1)
            times = 1
            if word == "times":
                word, line = line.split(maxsplit=1)
                times = int(word)
                word, line = line.split(maxsplit=1)

            if word != "word":
                raise ValueError()

            for _ in range(times):
                instrs, inc = self.translate_const_lit(line, addr_cnt)
                addr_cnt += inc
                res.extend(instrs)

            line = src.readline().strip()

        return res

    def parse_text_section(self, src: TextIOBase) -> list[Translator.InnerInstruction | Translator.Label]:
        res: list[Translator.InnerInstruction | Translator.Label] = []

        while line := src.readline():
            line = line.strip()
            if not line:
                continue
            if line.endswith(":"):
                label = Translator.Label.parse(self, line)
                if label.name in self.text_labels:
                    label = self.text_labels[label.name]
                else:
                    self.text_labels[label.name] = label
                res.append(label)
            else:
                instr = Translator.InnerInstruction.parse(self, line)
                if not instr.check_signature():
                    print(line)
                    raise ValueError()
                res.append(instr)

        return res

    def expand_immediate_value(
        self, instrs: list[Translator.InnerInstruction | Translator.Label]
    ) -> list[Translator.InnerInstruction | Translator.Label]:
        res: list[Translator.InnerInstruction | Translator.Label] = []

        for i in instrs:
            if isinstance(i, Translator.InnerInstruction) and i.opcode in [
                Opcode.ADD,
                Opcode.SUB,
                Opcode.DIV,
                Opcode.REM,
                Opcode.CMP,
            ]:
                cnt = 0
                instr = Translator.InnerInstruction(i.opcode, [])
                for arg in i.args:
                    if arg.arg_type in Translator.InnerArgType.INT | Translator.InnerArgType.LABEL:
                        res.append(
                            Translator.InnerInstruction(
                                Opcode.MOV,
                                [
                                    Translator.InnerArgument(Translator.InnerArgType.REG, self.reg_cnt - cnt),
                                    arg,
                                ],
                            )
                        )
                        instr.args.append(Translator.InnerArgument(Translator.InnerArgType.REG, self.reg_cnt - cnt))
                        cnt += 1
                    else:
                        instr.args.append(arg)
                res.append(instr)
            else:
                res.append(i)
        return res

    def eliminate_labels(
        self, instrs: list[Translator.InnerInstruction | Translator.Label], line_cnt: int
    ) -> list[Translator.InnerInstruction]:
        res: list[Translator.InnerInstruction] = []
        for i in instrs:
            if isinstance(i, Translator.Label):
                i.addr = line_cnt + len(res)
            else:
                res.append(i)
        return res

    def translate_text_section(self, src: TextIOBase, line_cnt: int) -> list[Instruction]:
        labels_and_instrs: list[Translator.InnerInstruction | Translator.Label] = self.parse_text_section(src)
        labels_and_instrs = self.expand_immediate_value(labels_and_instrs)

        inner_instrs = self.eliminate_labels(labels_and_instrs, line_cnt)

        res: list[Instruction] = []

        for i in inner_instrs:
            instr = Instruction(i.opcode, [])
            for arg in i.args:
                if arg.arg_type in Translator.InnerArgType.LABEL:
                    instr.args.append(Argument(ArgType.INT, arg.value.addr))  # type: ignore
                elif arg.arg_type == Translator.InnerArgType.INT:
                    instr.args.append(Argument(ArgType.INT, arg.value))  # type: ignore
                else:
                    instr.args.append(Argument(ArgType.REG, arg.value))  # type: ignore
            res.append(instr)

        return [Instruction(Opcode.JMP, [Argument(ArgType.INT, self.text_labels["_start"].addr)]), *res]

    def translate(self, src: TextIOBase) -> list[Instruction]:
        data = self.translate_data_section(src)
        text = self.translate_text_section(src, len(data) + 1)
        return data + text
